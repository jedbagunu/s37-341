//IMPORTS
const Course = require('../models/Course')


module.exports.addCourse =(data) => {
	if(data.isAdmin){
		let new_course = new Course({
			name:  data.course.name,
			description: data.course.description,
			price: data.course.price

		})
		return new_course.save().then((new_course, error)=>{
			if(error){
				return false
			}
			return {
				message: 'New course successfully created!'
			}
		})

	}
	//return Promise.resolve('User must be admin')-- 
	let message = Promise.resolve({
		message: 'User must be admin to access this.'
	})

	return message.then((value)=>{
		return value
	})
}
		

module.exports.getAllCourses =() => {
		return Course.find({}).then((result)=>{
			return result
		})
}

module.exports.getAllActive =()=>{
	return Course.find({isActive: true}).then((result)=>{
		return result
	})
}

module.exports.getCourse = (course_Id) => {
	return Course.findById(course_Id).then((result)=>{
		return result
	})
}

module.exports.updateCourse = (course_Id, new_data) => {
		return Course.findByIdAndUpdate(course_Id, {
			name: new_data.name,
			description: new_data.description,
			price: new_data.price
		}).then((updated_course, error) => {
			if (error) {
				return false
			}
			return {
				message: 'Course updated successfully'
			}
		})
}

module.exports.isActiveCourse = (course_Id, new_data) => {
	return Course.findByIdAndUpdate(course_Id, {
		isActive: false
	}).then((isActive_course, error) =>{
		if (error) {
			return false
		}
		return {
			message: 'The course has been archived successfully!'
		}


		
	})
}
