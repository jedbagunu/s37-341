//IMPORTS
const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth')

//check email controller
module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0){
			return true
			
		}
		return false

	})
}

//register a user
module.exports.register = (data) => { 
	let encrypted_password = bcrypt.hashSync(data.password, 10)
	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo: data.mobileNo,
		password: encrypted_password
	})	
	return new_user.save().then((created_user, error)=>{
		if(error){
			return false
		}
		return {
			message: 'User successfully registered!'
		}
	})
}

//login user
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result)=>{
		if(result == null){ //checking if user exist
			return {
				message: "User doesn't exist!"
			}
		}
		const is_password_correct = bcrypt.compareSync(data.password, result.password)
		 if(is_password_correct){
		 	return {
		 		access: auth.createAccessToken(result)
		 	}
		 }
		 return {
		 	message: 'Password incorrect!'
		 }
	})
}

//get a single user
module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, {firstName: 1, lastName: 1, email: 1, mobileNo:1, enrollments:1}).then((result) => {
			return result
	
	})
} 
//get all user
module.exports.getAllUsers =() => {
		return User.find({}).then((result)=>{
			return result
		})
}

//enroll a user
module.exports.enroll = async (data) => {
	// check if user done adding the course to its enrollments array
	let is_user_updated = await User.findById(data.userId).then((user) => {
		user.enrollments.push({
			courseId: data.courseId
		})
			return user.save().then((updated_user, error)=>{
				if(error) {
					return false
				}
					return true
			})
	})
	 // check if course is done adding user to its enrollees array
	 let is_course_updated = await Course.findById(data.courseId).then((course) => {
		course.enrollees.push({
			userId: data.userId
		})
			return course.save().then((updated_course, error)=>{
				if(error) {
					return false
				}
					return true
			})
	})



	  //check if both user and course have been updated successfully and return a success message
	 if(is_user_updated &&is_course_updated) {
	 	return {
	 		message: 'User enrollment is successful!'
	 	}
	 }
	 // if the enrollment failed, return 'something went wrong'
	 return{
	 	message: 'Something went wrong'
	 }
}