const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseController')
const auth = require('../auth')

//create single course
router.post('/create', auth.verify, (request, response)=>{
	const data = {
		course: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin 	
	}



	courseController.addCourse(data).then((result)=>{
		response.send(result)
	})
	
})


//get all courses
router.get('/', (request, response)=>{
	courseController.getAllCourses().then((result)=>{
		response.send(result)
	})
})




// get all active courses
router.get('/active', (request, response)=>{
	courseController.getAllActive().then((result)=>{
		response.send(result)
	})
})



// get a single course
router.get('/:courseId', (request, response) => {
	courseController.getCourse(request.params.courseId).then((result)=>{
		response.send(result)
	})
})

// update single course

router.patch('/:courseId/update', auth.verify, (request, response)=>{
	courseController.updateCourse(request.params.courseId, request.body).then((result)=>{
		response.send(result)
	})
})


router.patch('/:courseId/archive', auth.verify, (request, response) => {
	courseController.isActiveCourse(request.params.courseId).then((result)=>{
		response.send(result)
	})
})

module.exports = router